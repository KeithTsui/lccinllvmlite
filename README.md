# LCC In LLVM Lite

This project is to make an LLVM backend for [LCC ISA](https://www.amazon.com/Under-Hood-Anthony-Dos-Reis/dp/1793302898).

However the way this project construct is via applying `git diff` patches, i.e. `lcc.patch`. The original made project is my another project [LLBackendLab](https://gitlab.com/KeithTsui/llbackendlab), which contains more custom backends, including LCC, Cpu0, M88k etc.

The LCC ISA is a experimental ISA for educational purpose, therefore it tries to be as simple as possible to support C and C++, as showed in its book, [C and C++ under the hood](https://www.amazon.com/Under-Hood-Anthony-Dos-Reis/dp/1793302898).

## Documentation

* [LCC ISA Instroduction](#lcc-isa-introduction)
* [LCC Assembly and Instruction Set](#lcc-assembly-and-instruction-set)
* [LCC Execution Mode](#lcc-execution-mode)
* [Build Dependencies](#build-dependencies)
* [Build LCC Backend and Run Test](#build-lcc-backend-and-run-test)
* [Test LCC Backend with LCCToolchain](#test-lcc-backend-with-lcctoolchain)
* [Road Map](#road-map)

## LCC ISA Introduction

LCC ISA is a 16-bit little endian machine with one ALU, 8 general purpose registers(R0~R7), 2 special registers(PC, IR) and status word (nzcv flags). The memory address space is 2^16 in 8-bit Byte, which in its book it is 16-bit Byte, that is word addressable not byte addressable, for my implementation convenience, I modified this specification to 8-bit Byte addressable like common ISA, ARM, X86, RISCV, etc.

Because the registers are 16-bit, particularly PC and IR are 16-bits too, and Memory is 8-bit byte addressable, therefore the natural alignment for native data and binary code is 2 bytes.

Be careful when assemble assembly into binary code.

![LCC Machine](./docs/LCCMachine.png)

## LCC Assembly and Instruction Set
[LCC Instruction Set Summary](/docs/LCCInstructionSetSummary.pdf)


## LCC Execution Mode
![LCC Execution Mode](./docs/LCCExecutionMode.png)

## Build Dependencies
As LCC Backend is an in-source proejct of LLVM, i.e. part of LLVM, therefore this project has the same requirements as LLVM infrastructure to build, please visit [Getting Started with the LLVM System](https://llvm.org/docs/GettingStarted.html#requirements) for further information. In short, it requires `CMake`, `git`, `clang`, `clang++` or `gcc`, `g++`, etc. on Unix-like OS.

And the same way to build as LLVM Infrastructure, please visit [Building LLVM with CMake](https://llvm.org/docs/CMake.html) to how to build LLVM with CMake.. Remeber that this project is built with LLVM 12, but not test for other LLVM version yet.

## Build LCC Backend and Run Test
After your environment satisfies the build requirement, you can just run `bootstrap.sh` to build and test LCC backend.

The `bootstrap.sh` is the script to construct this project and run a small test on it.

```bash
$ sudo chmod +x bootstrap.sh
$ ./bootstrap.sh
    .text
    .file	"lcc.cpp"
    .globl	sum                             # -- Begin function sum
    .type	sum,@function
sum:                                    # @sum
# %bb.0:                                # %entry
    add %SP, %SP, -2
    str %FP, %SP, 0                         # 2-byte Folded Spill
    mvr %FP, %SP
    ldr %r0, %FP, 2
    ldr %r1, %FP, 4
    add %r0, %r0, %r1
    mvr %SP, %FP
    ldr %FP, %SP, 0                         # 2-byte Folded Reload
    add %SP, %SP, 2
    ret
.Lfunc_end0:
    .size	sum, .Lfunc_end0-sum
                                        # -- End function
    .globl	f                               # -- Begin function f
    .type	f,@function
f:                                      # @f
# %bb.0:                                # %entry
    add %SP, %SP, -6
    str %FP, %SP, 4                         # 2-byte Folded Spill
    str %LR, %SP, 2                         # 2-byte Folded Spill
    mvr %FP, %SP
    mvi %r1, 0
    str %r1, %FP, 0
    ldr %r0, %FP, 0
    cmp %r0, %r1
    brne .LBB1_2
    br .LBB1_1
.LBB1_1:                                # %if.then
    add %SP, %SP, -4
    mvr %r1, %SP
    mvi %r0, 2
    str %r0, %r1, 2
    mvi %r0, 1
    str %r0, %r1, 0
    jsr sum
    add %SP, %SP, 4
    str %r0, %FP, 0
    br .LBB1_2
.LBB1_2:                                # %if.end
    ldr %r0, %FP, 0
    mvr %SP, %FP
    ldr %LR, %SP, 2                         # 2-byte Folded Reload
    ldr %FP, %SP, 4                         # 2-byte Folded Reload
    add %SP, %SP, 6
    ret
.Lfunc_end1:
    .size	f, .Lfunc_end1-f
                                        # -- End function
    .ident	"clang version 12.0.1 (https://github.com/llvm/llvm-project.git fed41342a82f5a3a9201819a82bf7a48313e296b)"
    .section	".note.GNU-stack","",@progbits
```

## Test LCC Backend Output with [LCCToolchain](https://gitlab.com/KeithTsui/lcctoolchain)

```bash
$ sudo chmod +x LCCToolchainTest.sh
$ ./LCCToolchainTest.sh
emo: Sum of two and five.
input: Sumof2And5.a
output: Sumof2And5.e
What is loaded in memory: 0 -- 16
0a20 0a22 0110 02f0 01f0 00f0 0200 0500
Memory show end.
7
Registers:
R0: 7
R1: 5
R2: 0
R3: 0
R4: 0
R5: 0
R6: 0
R7: 0
IR: 1111000000000000 f000 PC: c
nzcv: 0000
Register show ended.

Demo: Seperated Assemble then link and execute.
cp: ../lccinllvm/testcases/lcc.a: No such file or directory
input: lcc.a
output: lcc.e
input: startup.a
output: startup.e
processing file: 1
processing file: 2
file processed: 2
What is loaded in memory: 0 -- 76
1648 00f0 be1d 807b 8cab 4261 4463 0110 4cad 806b a21d c0cf ba1d 847b 827f 8cab
00d2 4073 4061 0180 1602 000e bc1d 8ca3 02d0 4270 01d0 4070 ca4f a41d 4071 000e
4061 4cad 826f 846b a61d c0cf
Memory show end.
Registers:
R0: 3
R1: 2
R2: 0
R3: 0
R4: 0
R5: 0
R6: 0
R7: 2
IR: 1111000000000000 f000 PC: 4
nzcv: 0101
Register show ended.
```

## Road Map
Now, this LCC backend can support:
1. Arithmetic and Logic operations.
2. Native supported Integer data types.
3. Local variables.
4. Function call.
5. Control Flow. (if-else, while-loop, for-loop, etc.)
6. Assembly file output.
7. ELF object file output. 

In the near future, it will support more features:
1. Support ELF executable on LCC Simulator. (In progress)
2. Support ELF in LLD for LCC. (Linking).
3. Deep dive in various optimizations.

