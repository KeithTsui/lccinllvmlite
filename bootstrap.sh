#!/usr/bin/env bash

# clone llvm-project from github
# apply lcc backend patch
# build llvm with lcc backend only
# run testcase to generate lcc asm file from c++ source file.
git clone https://github.com/llvm/llvm-project.git lccinllvm && \
    cd lccinllvm && \
    git checkout release/12.x && \
    git checkout fed41342a82f5a3a9201819a82bf7a48313e296b && \
    git apply ../lcc.patch && \
    mkdir build && \
    cd build &&
    cmake -DCMAKE_BUILD_TYPE=Debug \
          -DLLVM_TARGETS_TO_BUILD='LCC' -DLLVM_ENABLE_PROJECTS="clang" \
          -DLLVM_PARALLEL_COMPILE_JOBS=4 -DLLVM_PARALLEL_LINK_JOBS=1 -G "Ninja" ../llvm && \
        cmake --build . && \
        cd ../../testcases && \
        ../lccinllvm/build/bin/clang -target msp430-unknown-linux-gnu -S -emit-llvm lcc.cpp && \
        ../lccinllvm/build/bin/llc -march=lcc -filetype=asm -O0 -fast-isel=false -global-isel=false -relocation-model=static lcc.ll  -o -

# cd ..
# # clone LCC Toolchain
# # build LCC Toolchain
# # run test
# git clone https://gitlab.com/KeithTsui/lcctoolchain && \
#     cd lcctoolchain && \
#     mkdir -p build/Debug && \
#     cmake -DCMAKE_BUILD_TYPE=Debug -G Ninja -B ./build/Debug -S . -DCMAKE_INSTALL_PREFIX:PATH=./Install && \
#     cmake --build build/Debug && \
#     cmake --install build/Debug

# cd test

# echo "Demo: Sum of two and five."
# ../build/Debug/bin/Asm Sumof2And5.a && \
#     ../build/Debug/bin/Sim Sumof2and5.e

# echo ""
# echo "Demo: Seperated Assemble then link and execute."
# ../build/Debug/bin/Asm lcc.a && \
#     ../build/Debug/bin/Asm startup.a && \
#     ../build/Debug/bin/linker startup.e lcc.e && \
#     ../build/Debug/bin/Sim a.e
