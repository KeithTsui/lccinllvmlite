# clone LCC Toolchain
# build LCC Toolchain
# run test
git clone https://gitlab.com/KeithTsui/lcctoolchain && \
    cd lcctoolchain && \
    mkdir -p build/Debug && \
    cmake -DCMAKE_BUILD_TYPE=Debug -G Ninja -B ./build/Debug -S . -DCMAKE_INSTALL_PREFIX:PATH=./Install && \
    cmake --build build/Debug && \
    cmake --install build/Debug

cd test

echo "Demo: Sum of two and five."
../build/Debug/bin/Asm Sumof2And5.a && \
    ../build/Debug/bin/Sim Sumof2and5.e

echo ""
echo "Demo: Seperated Assemble then link and execute."
cp -rf ../lccinllvm/testcases/lcc.a lcc.a
../build/Debug/bin/Asm lcc.a && \
    ../build/Debug/bin/Asm startup.a && \
    ../build/Debug/bin/linker startup.e lcc.e && \
    ../build/Debug/bin/Sim a.e
