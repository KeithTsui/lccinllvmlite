; ModuleID = 'lcc.cpp'
source_filename = "lcc.cpp"
target datalayout = "e-m:e-p:16:16-i32:16-i64:16-f32:16-f64:16-a:8-n8:16-S16"
target triple = "msp430-unknown-linux-gnu"

; Function Attrs: noinline nounwind optnone mustprogress
define dso_local signext i16 @sum(i16 signext %a, i16 signext %b) #0 {
entry:
  %a.addr = alloca i16, align 2
  %b.addr = alloca i16, align 2
  store i16 %a, i16* %a.addr, align 2
  store i16 %b, i16* %b.addr, align 2
  %0 = load i16, i16* %a.addr, align 2
  %1 = load i16, i16* %b.addr, align 2
  %add = add nsw i16 %0, %1
  ret i16 %add
}

; Function Attrs: noinline nounwind optnone mustprogress
define dso_local signext i16 @f() #0 {
entry:
  %a = alloca i16, align 2
  store i16 0, i16* %a, align 2
  %0 = load i16, i16* %a, align 2
  %cmp = icmp eq i16 %0, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call = call signext i16 @sum(i16 signext 1, i16 signext 2)
  store i16 %call, i16* %a, align 2
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %1 = load i16, i16* %a, align 2
  ret i16 %1
}

attributes #0 = { noinline nounwind optnone mustprogress "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 2}
!1 = !{!"clang version 12.0.1 (https://github.com/llvm/llvm-project.git fed41342a82f5a3a9201819a82bf7a48313e296b)"}
