extern "C" short sum(short a, short b) { return a + b; }

extern "C" short f() {
  short a = 0;
  if (a == 0) {
    a = sum(1, 2);
  }
  return a;
}
